from flask import *
from flask.ext.script import Manager
from flask.ext.bootstrap import Bootstrap
from flask.ext.moment import Moment
from flask.ext.wtf import Form
from wtforms import StringField, SubmitField
from wtforms.validators import *
from flask.ext.sqlalchemy import SQLAlchemy
from db import Customer
import os

basedir = os.path.abspath(os.path.dirname(__file__))
app = Flask(__name__)
app.config['SECRET_KEY'] = 'hard to guess string'
app.config['SQLALCHEMY_DATABASE_URI'] =\
    'sqlite:///' + os.path.join(basedir, 'line-up.db')
app.config['SQLALCHEMY_RECORD_QUERIES'] = True
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
manager = Manager(app)
bootstrap = Bootstrap(app)
moment = Moment(app)
db = SQLAlchemy(app)

class NameForm(Form):
    phone = StringField('Enter your phone', validators=[Length(min=10, max=10)])
    submit = SubmitField('Submit')


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500


@app.route('/', methods=['GET', 'POST'])
def index():
    form = NameForm()
    if form.validate_on_submit():
        data = Customer(phone=request.form['phone'])
        db.session.add(data)
        db.session.commit()
        return redirect(url_for('/'))
    return render_template('index.html', form=form)


@app.route('/show',methods=['GET', 'POST'])
def show():
    if request.method == 'POST':
        kkk = db.session.query(Customer).filter_by(phone=request.form['phone']).first()
        #kkk = Customer.query.filter_by(phone=request.form['phone']).first()
        db.session.delete(kkk)
        db.session.commit()
        return request.form['phone']
    #result = Customer.query.with_entities(Customer.id,Customer.time)
    return render_template('show.html', shows=Customer.query.order_by(Customer.id.asc()).all())


if __name__ == '__main__':
    manager.run()
