mysql -u root -ptoor << EOF
create database shop;
use shop;
SET NAMES utf8;
SET time_zone = '+08:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS customer;
CREATE TABLE customer (
  ID int(10) NOT NULL AUTO_INCREMENT,
  Phone text NOT NULL,
  Time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (ID)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS member;
CREATE TABLE member (
  ID int(11) NOT NULL AUTO_INCREMENT,
  name text COLLATE utf8_unicode_ci NOT NULL,
  sex text COLLATE utf8_unicode_ci NOT NULL,
  bday text COLLATE utf8_unicode_ci NOT NULL,
  phone text COLLATE utf8_unicode_ci NOT NULL,
  address text COLLATE utf8_unicode_ci NOT NULL,
  email text COLLATE utf8_unicode_ci NOT NULL,
  message text COLLATE utf8_unicode_ci,
  PRIMARY KEY (ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS smstext;
CREATE TABLE smstext (
  ID int(11) NOT NULL AUTO_INCREMENT,
  phone text NOT NULL,
  message text COLLATE utf8_unicode_ci NOT NULL,
  Time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

EOF
