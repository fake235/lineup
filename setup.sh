#install packge
apt-get install vim git nginx php5-fpm mysql-server php5-mysql python-mysqldb python-pip sudo firmware-ralink firmware-realtek wireless-tools wpasupplicant
pip install pyserial

#copy code to /var/www
mkdir /var/www
cp -r ./www/* /var/www/
chmod -R 777 /var/www
cp  ~/lineup/update.sh ~

#set nginx
rm /etc/nginx/sites-enabled/default
cp ./default /etc/nginx/sites-enabled/
echo "www-data ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
service nginx reload

#set wifi
echo -e "allow-hotplug wlan0\niface wlan0 inet manual\nwpa-roam /etc/wpa_supplicant/wpa_supplicant.conf\niface default inet dhcp" >> /etc/network/interfaces

echo -n "Enter wifi ssid > "
read text
echo -n "Enter wifi password > "
read passwd

echo "network={
    ssid=\"$text\"
    psk=\"$passwd\"
}" > /etc/wpa_supplicant/wpa_supplicant.conf


#set sql
./sql.sh

#set autorun
echo -e '#!/bin/sh -e\n# rc.local\nsudo python /var/www/autoSMS/lineUp_SendSMS_SQL.py &\nexit 0' > /etc/rc.local

reboot


