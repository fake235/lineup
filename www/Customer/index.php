<html>

<head>
    <?php include( "../static/css.php");?>
        <title>input Phone number</title>
</head>

<body>
    <?php include( "../static/bar.php");?>
        <div class="container">
            <div class="col-md-8 col-md-offset-4">
                <p>
                    <br>
                    <br>
                    <br>
                </p>
                <h2>請輸入您的手機號碼： </h2>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <div class="page-header"></div>
            </div>
            <div class="row" style="padding-bottom:10%;">
                <div class="col-md-8 col-md-offset-4">
                <div class="col-xs-4">
                    <input id="phone" type="text" class="form-control" name="phonenum" placeholder="EX:0912345678">
                </div>
                    <input id="send" type="button" class="btn btn-success" value="排隊" onClick="doStuff()">
                    <p>
                        <br>
                        <br>
                        <br>
                        <br>
                    </p>
                </div>
            </div>
        </div>
        <?php include "../static/footer.php";?>
</body>

</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
function doStuff() {
    var nameElement = document.getElementById("phone");
    var theName = nameElement.value;
    if ((theName.indexOf("09") == 0) && (theName.length == 10)) {
        $.post("Customer/InputPhoneT.php", {
                phonenum: $("#phone").val(),
            },
            function(data) {
                alert(data);
                window.location.reload("index.php");
            });
    } else {
        alert("輸入錯誤，請重新輸入\n輸入格式為：0912345678");
        window.location.reload("index.php");
    }
}
</script>
