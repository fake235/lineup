<html lang="en">

<head>
    <title>排隊簡訊系統</title>
    <?php include( "static/css.php");?>
        <link rel="stylesheet" href="static/owl.carousel.css">
        <link rel="stylesheet" href="static/owl.theme.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
        <style>
        #owl-demo .item img {
            display: block;
            width: 100%;
            height: auto;
        }

        a {
            color: #333333;
        }

        p {
            font-size: 18px;
        }

        .img-fixed-top {
            margin-top: -20px;
        }
        </style>
</head>

<body>

   <?php include( "static/bar.php");?>
        <div class="img-fixed-top">
            <div id="carousel-example-captions" class="carousel slide" data-ride="carousel">
                
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="index_1.jpg">
                    </div>
                    
                    </div>
                </div>

            </div>

        <div class="container">
            <div class="row" style="padding-bottom:4%;">
                <div class="col-md-8 col-md-offset-2">
                    <div class="page-header">
                        <h1>歡迎光臨</h1>
                    </div>
                    <div class="jumbotron">
                        <p>　　歡迎光臨本店！我們有排隊系統，只要輸入您的手機電話號碼後我們將會於有空位時以"簡訊"的方式通知您！</p>
                        <p>　　您也可以加入我們的會員已享有更多的優惠，我們將會不定期推出活動以電子郵件的方式通知會員！</p>
                        <P>　　若您對我們有任何意見歡迎與我們聯絡，詳細資訊都在"關於"</P>
                    </div>
                </div>
            </div>
        </div>
        <?php include "static/footer.php";?>
</body>

</html>
