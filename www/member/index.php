<html>

<head>
    <?php include( "../static/css.php");?>
</head>

<body>
    <?php include( "../static/bar.php");?>
        <div class="container">
            <div class="row" style="padding-bottom:4%;">
                <div class="col-md-8 col-md-offset-2">
                    <div class="page-header">
                        <h1>加入會員</h1>
                    </div>
                    <form id="form1">
                        <h4>姓名</h4>
                        <input type="text" id="name" class="form-control" placeholder="EX: 王小明">
                        <h4>性別</h4>
                        <label>
                            <input type="radio" name="sex" value="男性" checked> 男性</label>
                        <br>
                        <label>
                            <input type="radio" name="sex" value="女性"> 女性</label>
                        <h4>生日</h4>
                        <input type="date" id="bday">
                        <h4>電話</h4>
                        <input type="text" id="phone" class="form-control" placeholder="EX: 0912345678">
                        <h4>住址</h4>
                        <input type="text" id="address" class="form-control" placeholder=" ">
                        <h4>信箱</h4>
                        <input type="text" id="email" class="form-control" placeholder="EX: XXX@XXX.XXX">
                        <h4>意見</h4>
                        <textarea id="message" class="form-control" rows="4" placeholder=" "></textarea>
                        <br>
                        <br>
                        <button type="button" class="btn btn-success" id="sendmail">送出</button>
                    </form>
                </div>
            </div>
        </div>
        <?php include "../static/footer.php";?>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
$("#sendmail").click(function() {
    var form_name = document.getElementById('form1');
    if (form_name.sex.value != undefined) {
        if (form_name.sex.checked) {}
    }
    var sex_ok = form_name.sex.value;
    var name = (document.getElementById("name")).value;
    var phone = (document.getElementById("phone")).value;
    var email = (document.getElementById("email")).value;
    var address = (document.getElementById("address")).value;
    var bday = (document.getElementById("bday").value);
    var year = parseInt(bday.slice(0, 4));

    if ((name.length >= 2) && (phone.indexOf("09") == 0) && (phone.length == 10) && (email.indexOf("@") != -1) && (email.indexOf(".") != -1) && (email.length >= 12) && ((address.indexOf("路") != -1) || (address.indexOf("街") != -1)) && (address.indexOf("號") != -1) && (address.length >= 8) && (bday.length == 10) && (year >= 1915) && (year <= 2010)) {
        $.post("member/member.php", {
                n: $("#name").val(),
                s: sex_ok,
                b: $("#bday").val(),
                p: $("#phone").val(),
                a: $("#address").val(),
                e: $("#email").val(),
                m: $("#message").val()
            },
            function(data) {

                if (data == 1) {
                    alert("恭喜！註冊成功");
                    window.location.reload("index.php");
                }else if (data == 2) {
                    alert("該E-mail已被註冊，請重新填寫");
                }else if (data == 3) {
                    alert("該電話已被註冊，請重新填寫");
                }else{
                    alert("else");
                }
            });
    } else {
        alert("輸入錯誤請重新輸入");
    }
});
</script>
