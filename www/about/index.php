<html>

<head>
    <title>關於</title>
    <?php include( "../static/css.php");?>
</head>

<body>
    <?php include( "../static/bar.php");?>
        <div class="container" style="padding-bottom:50px">
            <div class="col-md-10 col-md-offset-1">
                <div class="page-header">
                    <h1>關於<small> About</small></h1>
                </div>
                <h2>公司緣由:</h2>
                <p>
                    公司創立於2015年, 由外資與本國資金合辦的公司. 主要從事電腦系統整合、儀器設備設計研發、自動控制、半導體設備改進業， 與本公司相關的合作領域為工業自動化、醫療設備自動化、胜肽合成儀自動化 、半導體設備監控等。
                </p>
                <br>
                <h2>研發項目:</h2>
                <ol>
                    <li>設備嵌入式控制器, 軟硬體設計開發</li>
                    <li>Windows 應用程式開發, APP 程式設計</li>
                    <li>設備外觀設計, 機構設計</li>
                </ol>
                <br>
            </div>
            <div class="col-md-5 col-md-offset-1">
                <h2>相關訊息:</h2>
                <address>
                    <strong>ARD BioMed Company Limited</strong>
                    <br> No.117, Zhuangjing 6th St., Zhubei City
                    <br> Hsinchu County 302, Taiwan (R.O.C.)
                    <br>
                    <abbr title="Phone">Phone:</abbr> +886 3 5505987
                    <br>FAX +886 3 5167153
                    <br> ID 24851607
                    <br>
                    <a href="mailto:#">Service@ARDBioMed.com</a>
                </address>
                <address>
                    <strong>亞德生醫股份有限公司</strong>
                    <br> 302 新竹縣竹北市莊敬六街117號
                    <br>
                    <abbr title="Phone">電話</abbr> +886 3 5505987
                    <br>傳真 +886 3 5167153
                    <br> 統編 24851607
                    <br>
                    <a href="mailto:#">Service@ARDBioMed.com</a>
                </address>
                <h4>我們的標語</h4>
                <blockquote>
                    <p>People Who Know System Inegration.</p>
                </blockquote>
                <h4>探索、創造、分享</h4>
            </div>
            <div class="col-md-5">
                <h3>聯絡我們</h3>
                <br>
                <h4>姓名</h4>
                <input type="text" id="name" class="form-control" placeholder="Name">
                <h4>電話</h4>
                <input type="text" id="phone" class="form-control" placeholder="phone">
                <h4>信箱</h4>
                <input type="text" id="email" class="form-control" placeholder="email">
                <h4>意見</h4>
                <textarea id="message" class="form-control" rows="4" placeholder="message"></textarea>
                <br>
                <button type="button" class="btn btn-success" id="sendmail">送出</button>
            </div>
        </div>
        <?php include( "../static/footer.php");?>
            <script>
            $("#sendmail").click(function() {
                $.post("mail.php", {
                        n: $("#name").val(),
                        p: $("#phone").val(),
                        e: $("#email").val(),
                        m: $("#message").val()
                    },
                    function(data) {
                        alert(data);
                    });
            });
            </script>
</body>

</html>