<div class="panel panel-default">
    <div class="panel-body" style="background-color: #cce0f2;">
        <div class="text-center">
            <p style="font-size:20px;font-weight: bold">
                <span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span> 聯絡我們
                <span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span> +886 3 5505987
            </p>
        </div>
    </div>
    <div class="panel-footer">
        <div class="text-center">
            亞德生醫股份有限公司
            <br>Service@ARDBioMed.com
        </div>
    </div>
</div>


<script>
/*
    jQuery(document).ready(function(){
        $(".dropdown").hover(
            function() { $('.dropdown-menu', this).stop().fadeIn("fast");
            },
            function() { $('.dropdown-menu', this).stop().fadeOut("fast");
        });
    });*/
function scrollToAnchor(aid) {
    var aTag = $("[name='" + aid + "']");
    $('html,body').animate({
        scrollTop: aTag.offset().top - 200

    }, 'slow');
}

$(".bookmark").click(function() {
    var bla = $(this).text();
    scrollToAnchor(bla);
});
</script>
